/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.Recibo;
import Vista.dlgVista;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

//TERMINE
/**
 *
 * @author _Enrique_
 */
public class control implements ActionListener {
    
    private dlgVista Vista;
    private Recibo recibo;
    
    public control(dlgVista vista, Recibo reci){
        
        this.recibo=reci;
        this.Vista=vista;
      
        //hacer que el controlador escuche los botones de la vista
        
        Vista.btnNuevo.addActionListener(this);
        Vista.btnGuardar.addActionListener(this);
        Vista.btnLimpiar.addActionListener(this);
        Vista.btnMostrar.addActionListener(this);
        Vista.btnCerrar.addActionListener(this);
        Vista.btnLimpiar.addActionListener(this);
        
     
        Vista.cmbTipo.addActionListener(this);
        
    }
    
    private void iniciarVista(){
    
        Vista.setTitle("Productos");
        Vista.setSize(800,800);
        Vista.setVisible(true);
        
    } 
    
    @Override
    public void actionPerformed(ActionEvent e){
        if(e.getSource()==Vista.btnNuevo){
            
            Vista.txtNum.setEnabled(true);
            Vista.txtNombre.setEnabled(true);
            Vista.txtDomicilio.setEnabled(true);
            Vista.txtFecha.setEnabled(true);
            Vista.txtCosto.setEnabled(true);
            Vista.txtKilo.setEnabled(true);    
            
            Vista.cmbTipo.setEnabled(true);
            
            Vista.btnCancelar.setEnabled(true);
            Vista.btnLimpiar.setEnabled(true);
            Vista.btnGuardar.setEnabled(true);
            Vista.btnMostrar.setEnabled(true);
            
        }
        if(e.getSource()==Vista.btnCerrar){
        int option=JOptionPane.showConfirmDialog(Vista,"¿Deseas salir?",
        "Decide", JOptionPane.YES_NO_OPTION);
         if(option==JOptionPane.YES_NO_OPTION){
             Vista.dispose();
             System.exit(0);

            }
           
        }
        if(e.getSource()==Vista.btnGuardar){
            
            Vista.txtNum.setEnabled(false);
            Vista.txtNombre.setEnabled(false);
            Vista.txtDomicilio.setEnabled(false);
            Vista.txtFecha.setEnabled(false);
            Vista.txtCosto.setEnabled(false);
            Vista.txtKilo.setEnabled(false);    
            
            Vista.cmbTipo.setEnabled(false);
            
            try{
                
                recibo.setNumRecibo(Integer.parseInt(Vista.txtNum.getText()));
                recibo.setNombre(Vista.txtNombre.getText());
                recibo.setDomicilio(Vista.txtDomicilio.getText());
                recibo.setFecha(Vista.txtFecha.getText());
                
                System.out.println(Vista.cmbTipo.getSelectedIndex());
                
                switch(recibo.getTipoServi()){
                    
                    case 0:
                        recibo.setCostoKilo(2);
                        break;
                    case 1:
                        recibo.setCostoKilo(3);
                        break;
                    case 2:
                        recibo.setCostoKilo(5);
                        break;
                }
               
                
                
                recibo.setKilowatsCo(Float.parseFloat(Vista.txtKilo.getText()));
                Limpiar();
                
            }catch(NumberFormatException ex){
                
                JOptionPane.showMessageDialog(Vista, "Surgió el siguiente error: "+ex.getMessage());
                      
            }
            
            
        }
        if(e.getSource()==Vista.btnCancelar){
            
            Vista.txtNum.setEnabled(false);
            Vista.txtNombre.setEnabled(false);
            Vista.txtDomicilio.setEnabled(false);
            Vista.txtFecha.setEnabled(false);
            Vista.txtCosto.setEnabled(false);
            Vista.txtKilo.setEnabled(false);    
            
            Vista.cmbTipo.setEnabled(false);
            
            
            Vista.btnCancelar.setEnabled(false);
            Vista.btnLimpiar.setEnabled(false);
            Vista.btnGuardar.setEnabled(false);
            Vista.btnMostrar.setEnabled(false);
            
            
        }
        if(e.getSource()==Vista.btnCancelar){
            Limpiar();
        }
        if(e.getSource()==Vista.btnMostrar){
            
            try{
                Vista.txtNum.setText(String.valueOf(recibo.getNumRecibo()));
                Vista.txtNombre.setText(String.valueOf(recibo.getNombre()));
                Vista.txtDomicilio.setText(String.valueOf(recibo.getDomicilio()));
                Vista.txtFecha.setText(String.valueOf(recibo.getFecha()));
                Vista.txtNum.setText(String.valueOf(recibo.getNumRecibo()));
                
                Vista.cmbTipo.setSelectedIndex(0);
                
                Vista.txtCosto.setText(String.valueOf(recibo.getCostoKilo()));
                
                Vista.txtSubtotal.setText(String.valueOf(recibo.calcularSub()));
                Vista.txtImpuesto.setText(String.valueOf(recibo.calcularImpu()));
                Vista.txtTotal.setText(String.valueOf(recibo.calcularTotal()));
                
                
                
            }catch(NumberFormatException ex){
                
                JOptionPane.showMessageDialog(Vista, "Surgió el siguiente error: "+ex.getMessage());
                
            }catch (Exception ex2){
                 JOptionPane.showMessageDialog(Vista, "Surgió el siguiente error: "+ex2.getMessage());
            }  
            
        }
    }
    
    public void Limpiar(){

       Vista.txtNum.setText("");
       Vista.txtNombre.setText("");
       Vista.txtDomicilio.setText("");
       Vista.txtFecha.setText("");
       Vista.txtCosto.setText("");
       Vista.txtKilo.setText("");
       
       Vista.txtSubtotal.setText("");
       Vista.txtImpuesto.setText("");
       Vista.txtTotal.setText("");
       

    }
    
    public static void main(String[] args) {
        // TODO code application logic here
       
        Recibo reci = new Recibo();
        dlgVista vista = new dlgVista(new JFrame(), true);
        control contro = new control(vista,reci);
        contro.iniciarVista();
    }
}
    
    