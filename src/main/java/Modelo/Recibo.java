/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author _Enrique_
 */
public class Recibo {
    
    int numRecibo,tipoServi;
    String nombre, domicilio, fecha;
    float costoKilo, kilowatsCo;
    
    public Recibo(){
        
        this.numRecibo=0;
        this.nombre="";
        this.domicilio="";
        this.tipoServi=0;
        this.fecha="";
        this.costoKilo=0.02f;
        this.kilowatsCo=0.02f;
               
    }
    
    public Recibo(Recibo Reci){
        
        this.numRecibo=Reci.numRecibo;
        this.nombre=Reci.nombre;
        this.domicilio=Reci.domicilio;
        this.tipoServi=Reci.tipoServi;
        this.fecha=Reci.fecha;
        this.costoKilo=Reci.costoKilo;
        this.kilowatsCo=Reci.kilowatsCo;
        
        
    }
    public Recibo(int numRe, String nombre, String domi, int tipoServi, String fecha, float costoK, float kilowatC){
        
        this.numRecibo=numRe;
        this.nombre=nombre;
        this.domicilio=domi;
        this.tipoServi=tipoServi;
        this.fecha=fecha;
        this.costoKilo=costoK;
        this.kilowatsCo=kilowatC;
        
        
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getTipoServi() {
        return tipoServi;
    }

    public void setTipoServi(int tipoServi) {
        this.tipoServi = tipoServi;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public float getCostoKilo() {
        return costoKilo;
    }

    public void setCostoKilo(float costoKilo) {
        this.costoKilo = costoKilo;
    }

    public float getKilowatsCo() {
        return kilowatsCo;
    }

    public void setKilowatsCo(float kilowatsCo) {
        this.kilowatsCo = kilowatsCo;
    }
    
    public float calcularSub(){
        
        float subTotal;
        
        subTotal= costoKilo*kilowatsCo;
        
        return subTotal;
        
    } 
    
    public float calcularImpu(){
        
        float impu;
        
        impu= (calcularSub()/100)*16;
        
        return impu;
        
    }
    
    public float calcularTotal(){
        
        float total;
        
        total= calcularSub() + calcularImpu();
     
        return total;
    }
    
    
    
}
